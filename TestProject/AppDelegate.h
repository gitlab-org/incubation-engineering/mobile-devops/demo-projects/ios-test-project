//
//  AppDelegate.h
//  TestProject
//
//  Created by Serhii Bilous on 22.05.2024.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;


@end

